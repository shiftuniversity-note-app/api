package main

import (
	"context"
	"log"
	"os"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
	"github.com/joho/godotenv"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Model struct {
	ID   string `json:"_id" bson:"_id"`
	Note string `json:"note" bson:"note"`
	V    int    `json:"_v" bson:"_v"`
}

type List struct {
	Notes []Model `json:"notes"`
}

type ENV struct {
	ENV string
}

func MongoClient() (*mongo.Client, context.Context) {

	// Local pc de calisiyorsan "ENV=local go test" ile calistiricaksin
	if os.Getenv("ENV") == "local" {

		err := godotenv.Load(".env")

		if err != nil {
			log.Fatalf("Error loading .env file")
		}
	}

	dbURL := os.Getenv("MONGO_URL")

	ctx, _ := context.WithTimeout(context.Background(), 15*time.Second)

	clientOptions := options.Client().ApplyURI(dbURL)
	client, _ := mongo.Connect(ctx, clientOptions)
	return client, ctx
}

func (env *ENV) GetHandler(c *fiber.Ctx) error {
	var notes *List
	var err error

	if env.ENV == "CDC" {
		notes, err = MockGetNotes()
	} else {
		notes, err = GetNotes()

	}

	c.JSON(notes)
	c.Status(fiber.StatusOK)
	return err
}

func (env *ENV) PostHandler(c *fiber.Ctx) error {

	model := Model{}

	errr := c.BodyParser(&model)

	if errr != nil {
		return errr
	}

	var data *Model
	var err error

	if env.ENV == "CDC" {
		data, err = MockAddNote()
	} else {
		data, err = AddNote(&model)
	}

	c.JSON(data)

	c.Status(fiber.StatusCreated)
	return err

}

func AddNote(m *Model) (*Model, error) {

	if m.Note == "" {
		return nil, fiber.NewError(404, "Note should not be empty")
	}

	client, ctx := MongoClient()
	db := client.Database("note").Collection("notes")
	if m.ID == " " {
		m.ID = uuid.New().String()
	}

	db.InsertOne(ctx, m)

	return GetNote(m.ID), nil
}

func GetNote(id string) *Model {
	client, ctx := MongoClient()
	db := client.Database("note").Collection("notes")

	emptyModel := Model{}

	db.FindOne(ctx, bson.M{"_id": id}).Decode(&emptyModel)

	return &emptyModel

}

func GetNotes() (*List, error) {
	client, ctx := MongoClient()
	db := client.Database("note").Collection("notes")

	emptyList := List{}

	cursor, err := db.Find(ctx, bson.M{})

	if err != nil {
		return nil, err
	}

	defer cursor.Close(ctx)

	for cursor.Next(ctx) {
		emptyModel := Model{}

		cursor.Decode(&emptyModel)
		emptyList.Notes = append(emptyList.Notes, emptyModel)
	}

	return &emptyList, nil
}
