package main

import (
	"os"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
)

func main() {

	app := fiber.New()
	app.Use(cors.New(cors.Config{
		AllowOrigins: "*",
		AllowHeaders: "Origin, Content-Type, Accept",
	}))
	app = Endpoints(app)

	app.Listen(":5000")
}

func Endpoints(app *fiber.App) *fiber.App {

	appENV := os.Getenv("TEST")

	env := ENV{ENV: appENV}

	app.Get("/notes", env.GetHandler)
	app.Post("/notes", env.PostHandler)

	return app
}
